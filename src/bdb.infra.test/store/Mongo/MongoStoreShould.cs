﻿using System;
using System.Collections;
using System.Linq;
using System.Threading.Tasks;
using bdb.infra.specs;
using bdb.infra.specs.Sorters;
using bdb.infra.specs.Specifications.ESpecifications;
using bdb.infra.store.Mongo;
using bdb.infra.test._Helpers;
using FakeItEasy;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;

namespace bdb.infra.test.store.Mongo
{
	[TestFixture]
	public class MongoStoreShould
	{
		MongoStore utMongoStore;

		[SetUp]
		public void Setup()
		{
			_dbName = string.Format("test-bdb-infra_{0}", RndGenerator.GetAlphaNumString(4));
			var mClient = new MongoClient(new MongoClientSettings
			{
				ClusterConfigurator = builder =>
				{
					builder.ConfigureCluster(settings => settings.With(serverSelectionTimeout: TimeSpan.FromSeconds(1)));
				}
			});
			bool isOff = false;
			try
			{
				_db = mClient.GetDatabase(_dbName);
				var pingTask = _db.RunCommandAsync<BsonDocument>(new BsonDocument("ping", 1));
				pingTask.Wait();
				if (pingTask.IsFaulted)
				{
					isOff = true;
				}
			}
			catch (AggregateException e)
			{
				Console.WriteLine(e.InnerException.Message);
				isOff = true;
			}
			if (isOff)
			{
				_db = null;
				_dbName = null;
				Assert.Ignore("MongoDb is off");
				return;
			}

			utMongoStore = new MongoStore(_db);
		}

		[Test]
		public async Task StoreEntityInDatabase()
		{
			string
				id = GenerateId(),
				name = GenerateName(),
				collectionName = typeof(TestClass).Name.ToLowerInvariant();
			TestClass entity = new TestClass { Id = id, Name = name };

			await utMongoStore.StoreAsync(entity);

			var collection = _db.GetCollection<TestClass>(collectionName);
			var entities = await collection.Find(new BsonDocument()).ToListAsync();
			Assert.AreEqual(1, entities.Count);
			Assert.AreEqual(id, entities[0].Id);
			Assert.AreEqual(name, entities[0].Name);
		}

		[Test]
		public async Task RemoveEntityInDatabaseById()
		{
			string
				id1 = GenerateId(), id2 = GenerateId(), id3 = GenerateId(),
				name1 = GenerateName(), name2 = GenerateName(), name3 = GenerateName(),
				collectionName = typeof(TestClass).Name.ToLowerInvariant();
			TestClass
				entity1 = new TestClass { Id = id1, Name = name1 },
				entity2 = new TestClass { Id = id2, Name = name2 },
				entity3 = new TestClass { Id = id3, Name = name3 },
				entityR = new TestClass { Id = id2, Name = name1 };
			var collection = _db.GetCollection<TestClass>(collectionName);
			await collection.InsertManyAsync(new TestClass[] { entity1, entity2, entity3 });

			await utMongoStore.RemoveAsync(entityR);

			var entities = await collection.Find(new BsonDocument()).ToListAsync();
			Assert.AreEqual(2, entities.Count);
			Assert.AreEqual(new string[] { id1, id3 }, entities.Select(e => e.Id).ToArray());
		}

		[Test]
		public async Task ReplaceEntityInDatabaseById()
		{
			string
				id1 = GenerateId(), id2 = GenerateId(),
				name1 = GenerateName(), name2 = GenerateName(), name3 = GenerateName(),
				collectionName = typeof(TestClass).Name.ToLowerInvariant();
			TestClass
				entity1 = new TestClass { Id = id1, Name = name1 },
				entity2 = new TestClass { Id = id2, Name = name2 },
				entityR = new TestClass { Id = id2, Name = name3 };
			var collection = _db.GetCollection<TestClass>(collectionName);
			await collection.InsertManyAsync(new TestClass[] { entity1, entity2 });

			await utMongoStore.StoreAsync(entityR);

			var entities = await collection.Find(new BsonDocument()).ToListAsync();
			Assert.AreEqual(2, entities.Count);
			Assert.AreEqual(new string[] { id1, id2 }, entities.Select(e => e.Id).ToArray());
			Assert.AreEqual(new string[] { name1, name3 }, entities.Select(e => e.Name).ToArray());
		}

		[Test]
		public async Task GetEntityFromDatabaseById()
		{
			string
				id1 = GenerateId(), id2 = GenerateId(),
				name1 = GenerateName(), name2 = GenerateName(),
				collectionName = typeof(TestClass).Name.ToLowerInvariant();
			TestClass
				entity1 = new TestClass { Id = id1, Name = name1 },
				entity2 = new TestClass { Id = id2, Name = name2 };
			var collection = _db.GetCollection<TestClass>(collectionName);
			await collection.InsertManyAsync(new TestClass[] { entity1, entity2 });

			var entity = await utMongoStore.GetByIdAsync<TestClass>(id1);

			Assert.AreEqual(id1, entity.Id);
			Assert.AreEqual(name1, entity.Name);
		}

		[Test]
		public async Task FindEntitiesByESpec()
		{
			string
				id1 = GenerateId(), id2 = GenerateId(),
				name1 = GenerateName(), name2 = GenerateName(),
				collectionName = typeof(TestClass).Name.ToLowerInvariant();
			TestClass
				entity1 = new TestClass { Id = id1, Name = name1 },
				entity2 = new TestClass { Id = id2, Name = name2 };
			var collection = _db.GetCollection<TestClass>(collectionName);
			await collection.InsertManyAsync(new TestClass[] { entity1, entity2 });
			ESpecification<TestClass> espec = A.Fake<ESpecification<TestClass>>();
			A.CallTo(() => espec.TargetExpression)
				.Returns(tc => tc.Name == name2);

			var entities = await utMongoStore.FindEntitiesAsync(espec);

			Assert.AreEqual(1, entities.Count);
			Assert.AreEqual(id2, entities.First().Id);
			Assert.AreEqual(name2, entities.First().Name);
		}

		[Test]
		public async Task FailWhenFindWithISpecNotESpec()
		{
			string
				id1 = GenerateId(), id2 = GenerateId(),
				name1 = GenerateName(), name2 = GenerateName(),
				collectionName = typeof(TestClass).Name.ToLowerInvariant();
			TestClass
				entity1 = new TestClass { Id = id1, Name = name1 },
				entity2 = new TestClass { Id = id2, Name = name2 };
			var collection = _db.GetCollection<TestClass>(collectionName);
			await collection.InsertManyAsync(new TestClass[] { entity1, entity2 });
			ISpecification<TestClass> ispec = A.Fake<ISpecification<TestClass>>();
			A.CallTo(() => ispec.IsSatisfied(A<TestClass>.Ignored))
				.Returns(true);

			// TODO: workaround for async () issue in NUnit3TestAdapter
			//Assert.Throws<ArgumentException>(async () => await utMongoStore.FindEntitiesAsync(ispec));
			Assert.Throws<AggregateException>(() => utMongoStore.FindEntitiesAsync(ispec).Wait());
		}

		[Test]
		public async Task FindPagedCollection()
		{
			string collectionName = typeof(TestClass).Name.ToLowerInvariant();
			int
				total = RndGenerator.GetNumberBetween(2, 500),
				size = RndGenerator.GetNumberBetween(1, total / 2),
				totalPages = (int)Math.Ceiling((double)total / size),
				page = RndGenerator.GetNumberBetween(0, totalPages - 1);
			TestClass[] entities = Enumerable.Range(0, total)
				.Select(i => new TestClass { Id = GenerateId(), Name = GenerateName() })
				.ToArray();
			ESpecification<TestClass> espec = A.Fake<ESpecification<TestClass>>();
			A.CallTo(() => espec.TargetExpression)
				.Returns(tc => true);
			var collection = _db.GetCollection<TestClass>(collectionName);
			await collection.InsertManyAsync(entities);

			var pagedCollection = await utMongoStore.FindEntitiesAsync(espec, page, size);

			Assert.AreEqual(page, pagedCollection.PageNumber);
			Assert.AreEqual(size, pagedCollection.PageSize);
			Assert.AreEqual(total, pagedCollection.TotalEntities);
			Assert.AreEqual(totalPages, pagedCollection.TotalPages);
			Assert.GreaterOrEqual(size, pagedCollection.Count);
		}

		[Test]
		public async Task FindSortedCollection()
		{
			string collectionName = typeof(TestClass).Name.ToLowerInvariant();
			int total = RndGenerator.GetNumberBetween(2, 99);
			TestClass[] entities = new[]
			{
				new TestClass { Id="abc", Name="zxy" },
				new TestClass { Id="dfe", Name="dfe" },
				new TestClass { Id="dfz", Name="dfe" },
				new TestClass { Id="zxy", Name="abc" },
			};
			ESpecification<TestClass> espec = A.Fake<ESpecification<TestClass>>();
			A.CallTo(() => espec.TargetExpression)
				.Returns(tc => true);
			var collection = _db.GetCollection<TestClass>(collectionName);
			await collection.InsertOneAsync(entities[1]);
			await collection.InsertOneAsync(entities[0]);
			await collection.InsertOneAsync(entities[2]);
			await collection.InsertOneAsync(entities[3]);
			Sorter<TestClass> sorter1 = Sorter<TestClass>.OrderBy(tc => tc.Id).Desc();
			Sorter<TestClass> sorter2 = Sorter<TestClass>.OrderBy(tc => tc.Name);
			Sorter<TestClass> sorter3 = Sorter<TestClass>.OrderBy(tc => tc.Name).Desc()
				.ThenBy(tc => tc.Id).Desc();
			TestClass[] expectedSort1 = new[] { entities[3], entities[2], entities[1], entities[0] };
			TestClass[] expectedSort2 = new[] { entities[3], entities[1], entities[2], entities[0] };
			TestClass[] expectedSort3 = new[] { entities[0], entities[2], entities[1], entities[3] };

			var sortedCollection1 = await utMongoStore.FindEntitiesAsync(espec, 0, total, sorter1);
			var sortedCollection2 = await utMongoStore.FindEntitiesAsync(espec, 0, total, sorter2);
			var sortedCollection3 = await utMongoStore.FindEntitiesAsync(espec, 0, total, sorter3);


			var compr = new TestComparer();
			CollectionAssert.AreEqual(expectedSort1, sortedCollection1, compr);
			CollectionAssert.AreEqual(expectedSort2, sortedCollection2, compr);
			CollectionAssert.AreEqual(expectedSort3, sortedCollection3, compr);
		}

		[TearDown]
		public void Teardown()
		{
			if (_db == null)
				return;
			utMongoStore = null;
			var mClient = new MongoClient();
			mClient.DropDatabaseAsync(_dbName)
				.Wait();
			_db = null;
			_dbName = null;
		}

		private string _dbName;
		private IMongoDatabase _db;

		private string GenerateId()
		{
			return RndGenerator.GetAlphaNumString(32);
		}

		private string GenerateName()
		{
			return RndGenerator.GetAlphaNumString(128);
		}

		class TestComparer : IComparer
		{
			public int Compare(object x, object y)
			{
				TestClass x1 = (TestClass)x;
				TestClass y1 = (TestClass)y;
				if (x1.Name == y1.Name && x1.Id == y1.Id)
					return 0;
				return x.GetHashCode() - y.GetHashCode();
			}
		}
	}
}
