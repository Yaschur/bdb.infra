﻿using System;
using System.Linq;

namespace bdb.infra.test._Helpers
{
	public static class RndGenerator
	{
		internal static int GetNumberBetween(int minIncluded, int maxIncluded)
		{
			return random.Next(minIncluded, maxIncluded + 1);
		}

		internal static string GetAlphaNumString(int size)
		{
			char[] str = Enumerable.Range(0, Math.Abs(size))
				.Select(x => getRandomChar())
				.ToArray();
			return new string(str);
		}

		internal static bool GetBoolean()
		{
			return 1 == random.Next(0, 2);
		}

		internal static DateTime GetDateBetween(DateTime beginIncluded, DateTime endIncluded)
		{
			int days = (int)(endIncluded - beginIncluded).TotalDays;

			return beginIncluded.AddDays(random.Next(0, days));
		}

		private static Random random = new Random();
		private const string alphaNums = "abcdefghijklmnopqrstuvwxyz0123456789";

		private static char getRandomChar()
		{
			char alphaNum = alphaNums[random.Next(0, alphaNums.Length)];
			return GetBoolean() ? char.ToUpper(alphaNum) : alphaNum;
		}

	}
}
