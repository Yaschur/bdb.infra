﻿using bdb.infra.specs;
using bdb.infra.specs.Sorters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace bdb.infra.store
{
	public interface IStore
	{
		Task StoreAsync<TEntity>(TEntity entity);

		Task RemoveAsync<TEntity>(TEntity entity);

		Task<TEntity> GetByIdAsync<TEntity>(object id);

		//Task<IQueryable<TEntity>> QueryAllEntitiesAsync<TEntity>();

		Task<IReadOnlyCollection<TEntity>> FindEntitiesAsync<TEntity>(ISpecification<TEntity> specification);

		Task<PagedCollection<TEntity>> FindEntitiesAsync<TEntity>(
			ISpecification<TEntity> specification,
			int page,
			int size,
			Sorter<TEntity> sorter = null
		);
	}
}
