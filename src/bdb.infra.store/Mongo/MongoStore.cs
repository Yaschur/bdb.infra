﻿using bdb.infra.specs;
using bdb.infra.specs.Extensions;
using bdb.infra.specs.Sorters;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace bdb.infra.store.Mongo
{
	public class MongoStore : IStore
	{
		public MongoStore(IMongoDatabase mongoDatabase)
		{
			// TODO: validate arg

			_database = mongoDatabase;
		}

		public async Task<TEntity> GetByIdAsync<TEntity>(object id)
		{
			var filter = GetFilter<TEntity>(id);
			string collectionName = GenerateCollectionName<TEntity>();
			return await _database.GetCollection<TEntity>(collectionName)
				.Find(filter)
				.FirstOrDefaultAsync();
		}

		//public Task<IQueryable<TEntity>> QueryAllEntitiesAsync<TEntity>()
		//{
		//	throw new NotImplementedException();
		//}

		public async Task<IReadOnlyCollection<TEntity>> FindEntitiesAsync<TEntity>(ISpecification<TEntity> specification)
		{
			//var filter = GetFilter(specification);
			string collectionName = GenerateCollectionName<TEntity>();

			var list = await _database.GetCollection<TEntity>(collectionName)
				//.Find(filter)
				.AsQueryable()
				.Where(specification.Map2Expression())
				.ToListAsync();

			return list as IReadOnlyCollection<TEntity>;
		}

		public async Task<PagedCollection<TEntity>> FindEntitiesAsync<TEntity>(
			ISpecification<TEntity> specification,
			int page,
			int size,
			Sorter<TEntity> sorter = null
		)
		{
			//var filter = GetFilter(specification);
			string collectionName = GenerateCollectionName<TEntity>();

			var query = _database.GetCollection<TEntity>(collectionName)
				//.Find(filter);
				.AsQueryable()
				.Where(specification.Map2Expression());
			long total = await query.CountAsync();

			if (sorter != null)
			{
				query = sorter.InjectTo(query);
				//var sortedQuery = sorter.Items[0].IsDescending ? query.SortByDescending(sorter.Items[0].TargetExpression)
				//	: query.SortBy(sorter.Items[0].TargetExpression);
				//for (int i = 1; i < sorter.Items.Length; i++)
				//{
				//	sortedQuery = sorter.Items[i].IsDescending ? sortedQuery.ThenByDescending(sorter.Items[i].TargetExpression)
				//	: sortedQuery.ThenBy(sorter.Items[i].TargetExpression);
				//}
				//query = sortedQuery;
			}

			PagedCollection<TEntity> result = new PagedCollection<TEntity>(
				await query
					.Skip(page * size)
					.Take(size)
					.ToListAsync(),
				total,
				page,
				size
			);

			return result;
		}

		public Task RemoveAsync<TEntity>(TEntity entity)
		{
			var filter = GetFilter(entity);
			string collectionName = typeof(TEntity).Name.ToString().ToLowerInvariant();
			return _database.GetCollection<TEntity>(collectionName)
				.DeleteOneAsync(filter);
		}

		public Task StoreAsync<TEntity>(TEntity entity)
		{
			var filter = GetFilter(entity);
			string collectionName = GenerateCollectionName<TEntity>();

			var options = new UpdateOptions { IsUpsert = true };
			return _database.GetCollection<TEntity>(collectionName)
				.ReplaceOneAsync(filter, entity, options);
		}

		private readonly IMongoDatabase _database;

		#region static stuff

		private static string GenerateCollectionName<TEntity>()
		{
			return typeof(TEntity).Name.ToString().ToLowerInvariant();
		}

		private static FilterDefinition<TEntity> GetFilter<TEntity>(TEntity entity)
		{
			BsonClassMap cmap = BsonClassMap.LookupClassMap(typeof(TEntity));
			string idName = cmap.IdMemberMap.ElementName;
			object idValue = cmap.IdMemberMap.Getter.Invoke(entity);
			FilterDefinitionBuilder<TEntity> builder = Builders<TEntity>.Filter;
			return builder.Eq(idName, idValue);
		}

		private static FilterDefinition<TEntity> GetFilter<TEntity>(object entityId)
		{
			BsonClassMap cmap = BsonClassMap.LookupClassMap(typeof(TEntity));
			string idName = cmap.IdMemberMap.ElementName;
			FilterDefinitionBuilder<TEntity> builder = Builders<TEntity>.Filter;
			return builder.Eq(idName, entityId);
		}

		private static FilterDefinition<TEntity> GetFilter<TEntity>(ISpecification<TEntity> specification)
		{
			Expression<Func<TEntity, bool>> specExpression = specification.Map2Expression();
			FilterDefinitionBuilder<TEntity> builder = Builders<TEntity>.Filter;
			return builder.Where(specExpression);
		}

		#endregion
	}
}
