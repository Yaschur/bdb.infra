﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace bdb.infra.store
{
	public class PagedCollection<TEntity> : IReadOnlyCollection<TEntity>
	{
		public PagedCollection(
			IReadOnlyCollection<TEntity> srcCollection,
			long totalEntities,
			int pageNumber,
			int pageSize
		)
		{
			_collection = srcCollection;
			TotalEntities = totalEntities;
			TotalPages = pageSize == 0 ? -1L : (long)Math.Ceiling((double)totalEntities / (double)pageSize);
			PageNumber = pageNumber;
			PageSize = pageSize;
		}

		public int Count { get { return _collection.Count; } }

		public readonly long TotalEntities;

		public readonly long TotalPages;

		public readonly int PageNumber;

		public readonly int PageSize;

		public IEnumerator<TEntity> GetEnumerator()
		{
			return _collection.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _collection.GetEnumerator();
		}

		private readonly IReadOnlyCollection<TEntity> _collection;
	}
}
